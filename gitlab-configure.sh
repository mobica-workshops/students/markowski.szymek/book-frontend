#!/usr/bin/env bash
set -e
echo "${EX_BOOK_VAULT_PASS}" > vaultpass
if [ -z "$CI_COMMIT_TAG" ]; then
    PROD_VERSION="latest"
else
    PROD_VERSION="$CI_COMMIT_TAG"
fi
ansible-galaxy install -r requirements.yml -p roles
ansible-playbook \
    --vault-id vaultpass \
    -i 'localhost,' \
    --connection=local \
    --extra-vars "helm_image_tag_staging=latest helm_image_tag_prod=${PROD_VERSION}" \
    gitlab-playbook.yml
